import React  from "react";
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';

const FileManager = (props: any) => {
  return (
    <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
    <Box sx={{backgroundColor:'#EEEEEE', p: 3, mt:6, height:800, width:1000,borderRadius:5, justifyContent: 'center'}}>
    <Typography   fontFamily="'Century Gothic', Arial, sans-serif" sx={{ ml: 12, fontSize: 22, flex: 1 }}>No files Found</Typography>
    <Typography   fontFamily="'Century Gothic', Arial, sans-serif" sx={{ ml: 12, fontSize: 12, flex: 1 }}>Click Upload Files to create something</Typography>

    </Box>
    </div>
    
    
  
);
};

export default FileManager;

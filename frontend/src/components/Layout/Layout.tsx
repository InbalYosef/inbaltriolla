import React, { Fragment } from "react";
import MiniDrawer from './drawer'
const Layout = (props: any) => {
  return (
    <Fragment>
      <main>{props.children}</main>
      <MiniDrawer></MiniDrawer>
    </Fragment>
  );
};

export default Layout;

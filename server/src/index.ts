const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors');
const multer = require('multer');
const upload = multer({ dest: 'uploads/' }); // Define a folder to temporarily store uploaded files

import { uploadFiles, uploadFilesController, listFilesController } from './controllers/filesController';

import { connectToMongoDB, closeMongoDBConnection } from './db/db'; // Import the MongoDB connection functions

const PORT = process.env.PORT || 3001;

const app = express();
app.use(cors());

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.get("/api", (req, res) => {
  res.json({ message: "Hello from server!" });
});

// Endpoints
app.post('/api/v1/files/upload', uploadFiles.array('files'), uploadFilesController);
app.get('/api/v1/files',listFilesController);

async function startServer() {
  try {
    await connectToMongoDB();
    console.log('Connected to MongoDB');

    app.listen(PORT, () => {
      console.log(`Server listening on ${PORT}`);
    });
  } catch (error) {
    console.error('Error starting the server:', error);
  }
}

startServer();

process.on('exit', () => {
  closeMongoDBConnection().then(() => {
    console.log('MongoDB connection closed');
  });
});

process.on('SIGINT', () => {
  closeMongoDBConnection().then(() => {
    console.log('MongoDB connection closed');
    process.exit(0);
  });
});

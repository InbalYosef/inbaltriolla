import { MongoClient } from 'mongodb';

// MongoDB connection settings
const mongoHost = '10.20.0.197';
const mongoPort = 27017;
const mongoUrl = `mongodb://${mongoHost}:${mongoPort}`;

let client: MongoClient | null = null;

// Function to establish a connection to MongoDB
export async function connectToMongoDB() {
  if (!client) {
    try {
      client = new MongoClient(mongoUrl);
      await client.connect();
      console.log('Connected to MongoDB');
    } catch (error) {
      console.error('Error connecting to MongoDB:', error);
      throw error;
    }
  }
  return client;
}

// Function to close the MongoDB connection
export async function closeMongoDBConnection() {
  if (client) {
    await client.close();
    console.log('MongoDB connection closed');
  }
}

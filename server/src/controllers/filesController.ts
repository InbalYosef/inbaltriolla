import { Request, Response } from 'express';
import multer from 'multer';
import mongoose, { Document } from 'mongoose';

// Define the File model
const FileSchema = new mongoose.Schema({
  fileName: String,
  fileUrl: String,
  uploadDate: Date,
});

interface IFile extends Document {
  fileName: string;
  fileUrl: string;
  uploadDate: Date;
}

const FileModel = mongoose.model<IFile>('File', FileSchema);

export const uploadFiles = multer({ dest: 'uploads/' });

export const uploadFilesController = async (req: Request, res: Response) => {
  try {
    const files = req.files as Express.Multer.File[];

    const uploadedFiles: IFile[] = [];

    for (const file of files) {
      const newFile = new FileModel({
        fileName: file.originalname,
        fileUrl: file.path,
        uploadDate: new Date(),
      });
      await newFile.save();
      uploadedFiles.push(newFile);
    }

    res.json(uploadedFiles);
  } catch (error) {
    console.error('Error uploading files:', error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

export const listFilesController = async (req: Request, res: Response) => {
  try {
    const files = await FileModel.find({}, 'fileName fileUrl uploadDate').exec();
    res.json(files);
  } catch (error) {
    console.error('Error listing files:', error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

// server/index.ts
const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors');
const PORT = process.env.PORT || 3001;
const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true,
}));
// Serve static files from the "public" directory
app.use(express.static("server/src/public"));
app.get("/api", (req, res) => {
    res.json({ message: "Hello from server!" });
});
// app.use(createPaintingsRouter());
//app.use("/paintings", createPaintingsRouter());
app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
});
//# sourceMappingURL=index.js.map
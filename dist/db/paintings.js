"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAllPaintingsLocal = exports.getAllPaintings = void 0;
const pool_1 = __importDefault(require("./pool"));
const paintings_json_1 = __importDefault(require("../db/localDB/paintings.json"));
const getAllPaintings = () => __awaiter(void 0, void 0, void 0, function* () {
    const { rows } = yield pool_1.default.query(`
    SELECT 
    id,
    painting_name as "paintingName",
    mime_type as "mimeType",
    description,
    image_path as "imagePath"
  FROM "inbalProject".paintings;
  `);
    return rows;
});
exports.getAllPaintings = getAllPaintings;
const getAllPaintingsLocal = () => {
    return paintings_json_1.default;
};
exports.getAllPaintingsLocal = getAllPaintingsLocal;
//# sourceMappingURL=paintings.js.map
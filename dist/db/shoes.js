"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAllShoesLocal = exports.getAllShoes = void 0;
const pool_1 = __importDefault(require("./pool"));
const shoes_json_1 = __importDefault(require("../db/localDB/shoes.json"));
const getAllShoes = () => __awaiter(void 0, void 0, void 0, function* () {
    const { rows } = yield pool_1.default.query(`
    SELECT 
    id,
    shoes_name as "shoesName",
    mime_type as "mimeType",
    description,
    image_path as "imagePath"
  FROM "inbalProject".shoes;
  `);
    return rows;
});
exports.getAllShoes = getAllShoes;
const getAllShoesLocal = () => {
    return shoes_json_1.default;
};
exports.getAllShoesLocal = getAllShoesLocal;
//# sourceMappingURL=shoes.js.map
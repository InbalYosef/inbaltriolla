"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const paintings_1 = require("../db/paintings");
const paintingshandler_1 = require("../controllers/paintingshandler");
const cache_1 = __importDefault(require("../cache"));
const Router = require("express");
const createPaintingsRouter = () => {
    const router = Router();
    // With DB version
    //  router.get('/', getPaintingsHandler(getAllPaintings));
    // Without DB version
    (0, paintingshandler_1.getPaintingsHandler)(paintings_1.getAllPaintingsLocal);
    router.get('/paintings', cache_1.default.middleware, (0, paintingshandler_1.getPaintingsHandler)(paintings_1.getAllPaintingsLocal));
    return router;
};
exports.default = createPaintingsRouter;
//# sourceMappingURL=painterRouter.js.map
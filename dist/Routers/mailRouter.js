"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const nodemailer_1 = __importDefault(require("nodemailer"));
const sendMailRouter = (0, express_1.Router)();
// Replace these with your own values
const PASSWORD = "iywvcutnoroqwmke";
const transporter = nodemailer_1.default.createTransport({
    service: "gmail",
    auth: {
        user: "yosef.inbal@gmail.com",
        pass: PASSWORD,
    },
});
sendMailRouter.post("/", (req, res) => {
    const { name, lastName, email, message, phone } = req.body;
    const mailOptions = {
        to: "yosef.inbal@gmail.com",
        subject: `New message from ${name} ${lastName}`,
        html: `
    <div style="font-family: Century Gothic, sans-serif; background-color: #fafafa; text-align: center; margin: 0 auto;" dir="ltr">
    <p style="font-size: 22px; background-color: #cb4ea1e8; color: white; padding: 10px 0; font-weight: 500; ">The message from ${name} ${lastName} :</p>
    <p style="font-size: 18px;">${message}</p>
    <br>
    <p style="font-size: 18px;"><strong>Contact Information:</strong></p>
    <p style="font-size: 16px;"><strong>Email:</strong> ${email}</p>
    <p style="font-size: 16px;"><strong>Phone:</strong> ${phone}</p>
  </div>
  `
    };
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error);
            res.status(500).send("Error sending email");
        }
        else {
            console.log("Email sent: " + info.response);
            res.status(200).send("Email sent");
        }
    });
});
exports.default = sendMailRouter;
//# sourceMappingURL=mailRouter.js.map
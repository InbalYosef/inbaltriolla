"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const shoes_1 = require("../db/shoes");
const shoesHandler_1 = require("../controllers/shoesHandler");
const Router = require("express");
const createShoesRouter = () => {
    const router = Router();
    // DB version
    //  router.get('/', getShoesHandler(getAllShoes));
    // Local DB version
    router.get('/', (0, shoesHandler_1.getShoesHandler)(shoes_1.getAllShoesLocal));
    return router;
};
exports.default = createShoesRouter;
//# sourceMappingURL=shoesRouter.js.map
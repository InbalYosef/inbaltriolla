"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("./App.css");
const react_1 = __importDefault(require("react"));
const Layout_1 = __importDefault(require("./components/Layout/Layout"));
const styles_1 = require("@mui/material/styles");
const theme_1 = __importDefault(require("./theme"));
function App() {
    return (<styles_1.ThemeProvider theme={theme_1.default}>
      <Layout_1.default>
      </Layout_1.default>
      </styles_1.ThemeProvider>);
}
exports.default = App;
//# sourceMappingURL=App.js.map
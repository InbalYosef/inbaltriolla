"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const Box_1 = __importDefault(require("@mui/material/Box"));
const Typography_1 = __importDefault(require("@mui/material/Typography"));
const FileManager = (props) => {
    return (<div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
    <Box_1.default sx={{ backgroundColor: '#EEEEEE', p: 3, mt: 6, height: 800, width: 1000, borderRadius: 5, justifyContent: 'center' }}>
    <Typography_1.default fontFamily="'Century Gothic', Arial, sans-serif" sx={{ ml: 12, fontSize: 22, flex: 1 }}>No files Found</Typography_1.default>
    <Typography_1.default fontFamily="'Century Gothic', Arial, sans-serif" sx={{ ml: 12, fontSize: 12, flex: 1 }}>Click Upload Files to create something</Typography_1.default>

    </Box_1.default>
    </div>);
};
exports.default = FileManager;
//# sourceMappingURL=FileManager.js.map
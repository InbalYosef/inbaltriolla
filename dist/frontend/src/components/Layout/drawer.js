"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const styles_1 = require("@mui/material/styles");
const Box_1 = __importDefault(require("@mui/material/Box"));
const Drawer_1 = __importDefault(require("@mui/material/Drawer"));
const AppBar_1 = __importDefault(require("@mui/material/AppBar"));
const Toolbar_1 = __importDefault(require("@mui/material/Toolbar"));
const List_1 = __importDefault(require("@mui/material/List"));
const CssBaseline_1 = __importDefault(require("@mui/material/CssBaseline"));
const Typography_1 = __importDefault(require("@mui/material/Typography"));
const ListItem_1 = __importDefault(require("@mui/material/ListItem"));
const ListItemButton_1 = __importDefault(require("@mui/material/ListItemButton"));
const ListItemIcon_1 = __importDefault(require("@mui/material/ListItemIcon"));
const ListItemText_1 = __importDefault(require("@mui/material/ListItemText"));
const Settings_1 = __importDefault(require("@mui/icons-material/Settings"));
const Flare_1 = __importDefault(require("@mui/icons-material/Flare"));
const Avatar_1 = __importDefault(require("@mui/material/Avatar"));
require("./drawer.css");
const avatar1_png_1 = __importDefault(require("./avatar1.png"));
const material_1 = require("@mui/material");
const FileManager_1 = __importDefault(require("../FileManager"));
const drawerWidth = 240;
const openedMixin = (theme) => ({
    width: drawerWidth,
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
    }),
    overflowX: 'hidden',
});
const closedMixin = (theme) => ({
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    backgroundColor: '#5565FF',
    width: `calc(${theme.spacing(7)} + 1px)`,
    [theme.breakpoints.up('sm')]: {
        width: `calc(${theme.spacing(8)} + 1px)`,
    },
});
const DrawerHeader = (0, styles_1.styled)('div')(({ theme }) => (Object.assign({ display: 'flex', alignItems: 'center', justifyContent: 'flex-end', padding: theme.spacing(0, 1) }, theme.mixins.toolbar)));
const AppBar = (0, styles_1.styled)(AppBar_1.default, {
    shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => (Object.assign(Object.assign({ zIndex: theme.zIndex.drawer - 1, transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }) }, (open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
    }),
})), { boxShadow: 'none' })));
const Drawer = (0, styles_1.styled)(Drawer_1.default, { shouldForwardProp: (prop) => prop !== 'open' })(({ theme, open }) => (Object.assign(Object.assign({ width: drawerWidth, flexShrink: 0, whiteSpace: 'nowrap', boxSizing: 'border-box', backgroundColor: '#5565FF', boxShadow: 'none' }, (open && Object.assign(Object.assign({}, openedMixin(theme)), { '& .MuiDrawer-paper': openedMixin(theme) }))), (!open && Object.assign(Object.assign({}, closedMixin(theme)), { '& .MuiDrawer-paper': closedMixin(theme) })))));
function MiniDrawer() {
    const theme = (0, styles_1.useTheme)();
    const [open, setOpen] = React.useState(false);
    const handleDrawerOpen = () => {
        setOpen(true);
    };
    const handleDrawerClose = () => {
        setOpen(false);
    };
    return (<Box_1.default sx={{ display: 'flex' }}>
      <CssBaseline_1.default />
      <Drawer variant="permanent" open={open}>
        <List_1.default>
          {[' Settings', 'Flarrred'].map((text, index) => (<ListItem_1.default key={text} disablePadding sx={{ display: 'block' }}>
              <ListItemButton_1.default sx={{
                minHeight: 48,
                justifyContent: open ? 'initial' : 'center',
                px: 2.5,
                marginY: 5,
            }}>
                <ListItemIcon_1.default sx={{
                minWidth: 0,
                mr: open ? 3 : 'auto',
                justifyContent: 'center',
                color: 'white',
            }}>
                  {index % 2 === 0 ? <Settings_1.default /> : <Flare_1.default />}
                </ListItemIcon_1.default>
                <ListItemText_1.default primary={text} sx={{ opacity: open ? 1 : 0 }}/>
              </ListItemButton_1.default>
            </ListItem_1.default>))}
        </List_1.default>
      </Drawer>
      <AppBar sx={{ backgroundColor: '#FFFFFF', boxShadow: 'none', width: '100%' }} position="fixed" elevation={0}>
        <Toolbar_1.default sx={{ display: 'flex', justifyContent: 'flex-end', width: '100%' }}>
          <Avatar_1.default alt="User Avatar" src={avatar1_png_1.default}/>
        </Toolbar_1.default>
      </AppBar>
      <Box_1.default component="main" sx={{ flexGrow: 1, p: 3, display: 'flex', flexDirection: 'column' }}>
        <DrawerHeader />
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
          <Typography_1.default fontFamily="'Century Gothic', Arial, sans-serif" sx={{ ml: 12, fontSize: 22, flex: 1 }}>Files Manager</Typography_1.default>
          <material_1.Button sx={{
            backgroundColor: '#5565FF',
            borderRadius: 2.5,
            marginRight: 8,
            textTransform: 'none',
            width: '10%',
        }}>
             <Typography_1.default fontFamily="'Century Gothic', Arial, sans-serif" sx={{ fontSize: 12, flex: 1 }}>+ Upload Files</Typography_1.default>
  
          </material_1.Button>
        </div>
        <FileManager_1.default />
      </Box_1.default>
    </Box_1.default>);
}
exports.default = MiniDrawer;
//# sourceMappingURL=drawer.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// backend/cache.js
const memoryCache = require('memory-cache');
// Create an in-memory cache with a 1-minute expiration time
const myCache = new memoryCache.Cache();
const cache = {
    middleware: (req, res, next) => {
        const cachedData = myCache.get('paintingsData');
        if (cachedData) {
            console.log('Data fetched from cache');
            res.json(cachedData);
        }
        else {
            next();
        }
    },
    put: (key, data, duration) => {
        myCache.put(key, data, duration);
    },
};
exports.default = cache;
//# sourceMappingURL=cache.js.map
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.closeMongoDBConnection = exports.connectToMongoDB = void 0;
const mongodb_1 = require("mongodb");
// MongoDB connection settings
const mongoHost = '10.20.0.197';
const mongoPort = 27017;
const mongoUrl = `mongodb://${mongoHost}:${mongoPort}`;
let client = null;
// Function to establish a connection to MongoDB
function connectToMongoDB() {
    return __awaiter(this, void 0, void 0, function* () {
        if (!client) {
            try {
                client = new mongodb_1.MongoClient(mongoUrl);
                yield client.connect();
                console.log('Connected to MongoDB');
            }
            catch (error) {
                console.error('Error connecting to MongoDB:', error);
                throw error;
            }
        }
        return client;
    });
}
exports.connectToMongoDB = connectToMongoDB;
// Function to close the MongoDB connection
function closeMongoDBConnection() {
    return __awaiter(this, void 0, void 0, function* () {
        if (client) {
            yield client.close();
            console.log('MongoDB connection closed');
        }
    });
}
exports.closeMongoDBConnection = closeMongoDBConnection;
//# sourceMappingURL=db.js.map
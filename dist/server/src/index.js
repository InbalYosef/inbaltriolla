"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors');
const multer = require('multer');
const upload = multer({ dest: 'uploads/' }); // Define a folder to temporarily store uploaded files
const filesController_1 = require("./controllers/filesController");
const db_1 = require("./db/db"); // Import the MongoDB connection functions
const PORT = process.env.PORT || 3001;
const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true,
}));
app.get("/api", (req, res) => {
    res.json({ message: "Hello from server!" });
});
// Endpoints
app.post('/api/v1/files/upload', filesController_1.uploadFiles.array('files'), filesController_1.uploadFilesController);
app.get('/api/v1/files', filesController_1.listFilesController);
function startServer() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield (0, db_1.connectToMongoDB)();
            console.log('Connected to MongoDB');
            app.listen(PORT, () => {
                console.log(`Server listening on ${PORT}`);
            });
        }
        catch (error) {
            console.error('Error starting the server:', error);
        }
    });
}
startServer();
process.on('exit', () => {
    (0, db_1.closeMongoDBConnection)().then(() => {
        console.log('MongoDB connection closed');
    });
});
process.on('SIGINT', () => {
    (0, db_1.closeMongoDBConnection)().then(() => {
        console.log('MongoDB connection closed');
        process.exit(0);
    });
});
//# sourceMappingURL=index.js.map
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.listFilesController = exports.uploadFilesController = exports.uploadFiles = void 0;
const multer_1 = __importDefault(require("multer"));
const mongoose_1 = __importDefault(require("mongoose"));
// Define the File model
const FileSchema = new mongoose_1.default.Schema({
    fileName: String,
    fileUrl: String,
    uploadDate: Date,
});
const FileModel = mongoose_1.default.model('File', FileSchema);
exports.uploadFiles = (0, multer_1.default)({ dest: 'uploads/' });
const uploadFilesController = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const files = req.files;
        const uploadedFiles = [];
        for (const file of files) {
            const newFile = new FileModel({
                fileName: file.originalname,
                fileUrl: file.path,
                uploadDate: new Date(),
            });
            yield newFile.save();
            uploadedFiles.push(newFile);
        }
        res.json(uploadedFiles);
    }
    catch (error) {
        console.error('Error uploading files:', error);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});
exports.uploadFilesController = uploadFilesController;
const listFilesController = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const files = yield FileModel.find({}, 'fileName fileUrl uploadDate').exec();
        res.json(files);
    }
    catch (error) {
        console.error('Error listing files:', error);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});
exports.listFilesController = listFilesController;
//# sourceMappingURL=filesController.js.map